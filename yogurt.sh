#!/usr/bin/env bash

function downloadFiles() {
  printf "Downloading package-query and yaourt.\n"
  curl \
    -O https://aur.archlinux.org/cgit/aur.git/snapshot/package-query.tar.gz \
    -O https://aur.archlinux.org/cgit/aur.git/snapshot/yaourt.tar.gz &

  wait
}

function installPackageQuery() {
  printf "Installing package-query.\n"
  tar -xf package-query.tar.gz
  cd package-query
  yes | makepkg -is
  cd ..
}

function installYaourt() {
  printf "Installing yaourt.\n"
  tar -xf yaourt.tar.gz
  cd yaourt
  yes | makepkg -is
  cd ..
}

function removeFiles() {
  rm -rf {package-query,yaourt}{.tar.gz,}
}

downloadFiles
installPackageQuery
installYaourt
removeFiles
